package taskTwo;

import java.util.Random;

public class TaskTwoMain {
	
	public static void bubbleSort(int[] numArray) {

	    int n = numArray.length;
	    int temp = 0;

	    for (int i = 0; i < n; i++) {
	        for (int j = 1; j < (n - i); j++) {

	            if (numArray[j - 1] > numArray[j]) {
	                temp = numArray[j - 1];
	                numArray[j - 1] = numArray[j];
	                numArray[j] = temp;
	            }

	        }
	    }
	}

	public static void mergeSort(int inputArr[]) {
		int[] array = inputArr;
		int[] tempMergArr = new int[inputArr.length];
		int length = inputArr.length;
		tempMergArr = new int[length];

		int lowerIndex = 0;
		int higherIndex = inputArr.length - 1;
		doMergeSort(lowerIndex, higherIndex, tempMergArr, array);

	}

	private static void doMergeSort(int lowerIndex, int higherIndex, int[] tempMergArr, int[] array) {

		if (lowerIndex < higherIndex) {
			int middle = lowerIndex + (higherIndex - lowerIndex) / 2;

			doMergeSort(lowerIndex, middle, tempMergArr, array);

			doMergeSort(middle + 1, higherIndex, tempMergArr, array);

			mergeParts(lowerIndex, middle, higherIndex, tempMergArr, array);
		}
	}

	private static void mergeParts(int lowerIndex, int middle, int higherIndex, int[] tempMergArr, int[] array) {

		for (int i = lowerIndex; i <= higherIndex; i++) {
			tempMergArr[i] = array[i];
		}
		int i = lowerIndex;
		int j = middle + 1;
		int k = lowerIndex;
		while (i <= middle && j <= higherIndex) {
			if (tempMergArr[i] <= tempMergArr[j]) {
				array[k] = tempMergArr[i];
				i++;
			} else {
				array[k] = tempMergArr[j];
				j++;
			}
			k++;
		}
		while (i <= middle) {
			array[k] = tempMergArr[i];
			k++;
			i++;
		}
	}

	private static void quickSortInPlace(int[] S, int a, int b) {
		if (a >= b)
			return;

		int left = a;
		int right = b - 1;
		int pivot = S[b];
		int temp;

		while (left <= right) {

			while (left <= right && S[left] < pivot)
				left++;

			while (left <= right && S[right] > pivot)
				right--;
			if (left <= right) {

				temp = S[left];
				S[left] = S[right];
				S[right] = temp;
				left++;
				right--;
			}
		}
		temp = S[left];
		S[left] = S[b];
		S[b] = temp;

		quickSortInPlace(S, a, left - 1);
		quickSortInPlace(S, left + 1, b);
	}

	public static void main(String[] args) {
		
		int[] numbers = new int[100000];
		
		Random random = new Random();
		
		for (int i = 0; i < numbers.length; i++) {
			numbers[i] = random.nextInt(200000);
		}
		
		int[] numbersQuickSort = numbers.clone();
		int[] numbersMergeSort = numbers.clone();
		int[] numbersBubbleSort = numbers.clone();
		
		long start = System.nanoTime();
		quickSortInPlace(numbersQuickSort, 0, 99999);
		long stop = System.nanoTime();
		
		System.out.println("Quicksort time: " + (stop-start));
		
		start = System.nanoTime();
	    mergeSort(numbersMergeSort);
		stop = System.nanoTime();
		
		System.out.println("Mergesort time: " + (stop-start));
		
		start = System.nanoTime();
		bubbleSort(numbersBubbleSort);
		stop = System.nanoTime();
		
		System.out.println("Bubble time: " + (stop-start));
	}

}
