package taskOne;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class TaskOneMain {
	
	private static void quickSortInPlace(String[] S, int a, int b) {
		if (a >= b)
			return;

		int left = a;
		int right = b - 1;
		int pivot = S[b].length();
		String temp;
		
		while (left <= right) { 
								
			while (left <= right && S[left].length() < pivot)
				left++;
			
			
			while (left <= right && S[right].length() > pivot)
				right--;
			if (left <= right) { 
			
				temp = S[left];
				S[left] = S[right];
				S[right] = temp;
				left++;
				right--;
			}
		} 
		temp = S[left];
		S[left] = S[b];
		S[b] = temp;

		quickSortInPlace(S, a, left - 1);
		quickSortInPlace(S, left + 1, b);
	}


	public static void main(String[] args) throws IOException {

		File file = new File("resources/TextFile");

		BufferedReader bReader = new BufferedReader(new FileReader(file));

		String text = null;
		String line = bReader.readLine();

		while (line != null) {
			text = text + line;
			line = bReader.readLine();
		}

		bReader.close();

		text = text.trim().replaceAll("[^\\w\\s]", "").toLowerCase();

		String[] textArr = new String[200];
		textArr = text.split(" ");
		
		quickSortInPlace(textArr, 0, 199);
		
		for (int i = 0; i<200; i++) {
			System.out.println(textArr[i]);
		}
	}
}
