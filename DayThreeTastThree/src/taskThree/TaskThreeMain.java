package taskThree;

import java.util.Random;

public class TaskThreeMain {

	public static int findKthLargest(int[] nums, int k) {
		int start = 0, end = nums.length - 1, index = k;

		while (start < end) {
			int pivot = partion(nums, start, end);

			if (pivot < index)
				start = pivot + 1;
			else if (pivot > index)
				end = pivot - 1;
			else
				return nums[pivot];
		}
		return nums[start];
	}

	private static int partion(int[] nums, int start, int end) {
		int pivot = start;
		int temp;

		while (start <= end) {
			while (start <= end && nums[start] <= nums[pivot])
				start++;
			while (start <= end && nums[end] > nums[pivot])
				end--;

			if (start > end)
				break;

			temp = nums[start];
			nums[start] = nums[end];
			nums[end] = temp;
		}

		temp = nums[end];
		nums[end] = nums[pivot];
		nums[pivot] = temp;

		return end;
	}

	public static void main(String[] args) {
		int[] numbers = new int[1000000];
		Random random = new Random();

		long time;

		for (int i = 0; i < numbers.length; i++) {
			numbers[i] = random.nextInt(2000000);
		}

		for (int i = 0; i <= 50000;) {

			if (i == 10000) {
				time = System.currentTimeMillis();
				System.out.println("10k Step: " + findKthLargest(numbers, i) + "The time for 10 000 element: "
						+ (System.currentTimeMillis() - time));
			} else {
				System.out.println("10k Step: " + findKthLargest(numbers, i));
			}

			i += 10000;
		}

		for (int i = 50000; i <= 500000;) {

			if (i == 50000) {
				time = System.currentTimeMillis();
				System.out.println("50k Step: " + findKthLargest(numbers, i) + "The time for 50 000 element: "
						+ (System.currentTimeMillis() - time));
			} else {
				System.out.println("50k Step: " + findKthLargest(numbers, i));
			}

			i += 50000;
		}

		for (int i = 500000; i <= numbers.length - 1;) {

			if (i == 500000) {
				time = System.currentTimeMillis();
				System.out.println("100000k Step: " + findKthLargest(numbers, i) + "The time for 500 000 element: "
						+ (System.currentTimeMillis() - time));
			} else if (i==1000000) {
				time = System.currentTimeMillis();
				System.out.println("100000k Step: " + findKthLargest(numbers, i) + "The time for 1 000 000 element: "
						+ (System.currentTimeMillis() - time));
			} else {
				System.out.println("100000k Step: " + findKthLargest(numbers, i));
			}
			i += 100000;
		}

	}

}
